﻿namespace PrototypePattern.Common
{
    public interface IMyCloneable<T>
    {
        T Copy();
    }

}
