using PrototypePattern.Domain;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

var animal = new Animal(1, "Zebra", "wild animal");
var pet = new Pet(2, "Kesha", "birds", "zooVet");
var cat = new Cat(3, "Garfield", "bengal cat", "zooVet", "MBA");

Console.WriteLine($"�������� 1: \n{animal}\n");
Console.WriteLine($"�������� 2: \n{pet}\n");
Console.WriteLine($"�������� 3: \n{cat}\n\n");

var copyAnimal1 = animal.Copy();
var copyAnimal2 = pet.Copy();
var copyAnimal3 = cat.Copy();

Console.WriteLine($"����� ��������� 1: \n{copyAnimal1}\n");
Console.WriteLine($"����� ��������� 2: \n{copyAnimal2}\n");
Console.WriteLine($"����� ��������� 3: \n{copyAnimal3}\n\n");

var cloneAnimal1 = animal.Clone();
var cloneAnimal2 = pet.Clone();
var cloneAnimal3 = cat.Clone();

Console.WriteLine($"���� ��������� 1: \n{cloneAnimal1}\n");
Console.WriteLine($"���� ��������� 2: \n{cloneAnimal2}\n");
Console.WriteLine($"���� ��������� 3: \n{cloneAnimal3}\n\n");

app.Run();

