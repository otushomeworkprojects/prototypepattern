﻿using PrototypePattern.Common;
using System.Security.Cryptography;

namespace PrototypePattern.Domain
{
    public class Pet : Animal, IMyCloneable<Pet>, ICloneable
    {
        public Pet(int id, string name, string species, string vet)
            : base(id, name, species)
        {
            Vet = vet;
        }
        public string? Vet { get; set; }
        public override string ToString()
            => $"Pet '{Name}' ({Species}/{Id}) with vet '{Vet}'";

        public Pet Copy()
        {
            return new Pet(Id, Name, Species, Vet);
        }

        public object Clone()
        {
            return Copy();
        }
    }
}
