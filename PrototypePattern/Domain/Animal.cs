﻿using PrototypePattern.Common;

namespace PrototypePattern.Domain
{
    public class Animal : IMyCloneable<Animal>, ICloneable
    {
        public Animal(int id, string name, string species)
        {
            Name = name;
            Species= species;
            Id= id;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Species { get; set; }

        public object Clone()
        {
            return Copy();
        }

        public Animal Copy()
        {
            return new Animal(Id, Name, Species);
        }

        public override string ToString()
            => $"Animal '{Name}' ({Species}/{Id})";
    }
}
