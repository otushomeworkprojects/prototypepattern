﻿using PrototypePattern.Common;

namespace PrototypePattern.Domain
{
    public class Cat : Pet, IMyCloneable<Cat>, ICloneable
    {
        public Cat(int id, string name, string species, string vet, string educationLevel)
            : base(id, name, species, vet)
        {
            EducationLevel = educationLevel;
        }

        public string EducationLevel { get; set; }
        public override string ToString()
            => $"Cat '{Name}' ({Species}/{Id}) with education '{EducationLevel}'";

        public Cat Copy()
        {
            return new Cat(Id, Name, Species, Vet, EducationLevel);
        }

        public object Clone()
        {
            return Copy();
        }
    }

}
